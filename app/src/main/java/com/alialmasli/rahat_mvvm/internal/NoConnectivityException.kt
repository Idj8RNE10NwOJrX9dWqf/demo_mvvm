package com.alialmasli.rahat_mvvm.internal

import java.io.IOException

class NoConnectivityException:IOException()