package com.alialmasli.rahat_mvvm.data.db.entity

data class Request(
    val language: String,
    val query: String,
    val type: String,
    val unit: String
)