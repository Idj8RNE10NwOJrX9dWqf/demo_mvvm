package com.alialmasli.rahat_mvvm.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.alialmasli.rahat_mvvm.data.db.entity.CURRENT_WEATHER_ID
import com.alialmasli.rahat_mvvm.data.db.entity.CurrentWeatherEntry
import com.alialmasli.rahat_mvvm.data.db.unitlocalized.ImperialCurrentWeatherEntry

@Dao
interface CurrentWeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(weatherEntity: CurrentWeatherEntry)

    @Query("select * from current_weather where id = $CURRENT_WEATHER_ID")
    fun getWeather() : LiveData<ImperialCurrentWeatherEntry>
}