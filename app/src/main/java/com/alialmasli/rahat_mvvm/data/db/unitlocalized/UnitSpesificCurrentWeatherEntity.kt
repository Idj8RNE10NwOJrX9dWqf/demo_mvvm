package com.alialmasli.rahat_mvvm.data.db.unitlocalized

interface UnitSpesificCurrentWeatherEntity {
    val temperature: Double
    val conditionText: String
    val conditionIconUrl: String
    val windSpeed: Double
    val windDirection: String
    val precipitationVolume: Double
    val feelsLikeTemperature: Double
    val visibilityDistance: Double
}