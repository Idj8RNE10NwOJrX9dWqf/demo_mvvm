package com.alialmasli.rahat_mvvm.data.network

import androidx.lifecycle.LiveData
import com.alialmasli.rahat_mvvm.data.network.response.CurrentWeatherResponse

interface WeatherNetworkDataSource {
    val downloadedCurrentWeather: LiveData<CurrentWeatherResponse>

    suspend fun fetchCurrentWeather(location: String, languageCode: String)
}